console.log('----------------------')
console.log('Soal 1 Looping While')
console.log('Looping Pertama')
console.log('----------------------')
var flag = 2;
while(flag < 21) { 
  console.log(flag+' - I Love Coding'); 
  flag+=2; 
}
console.log('----------------------')
console.log('Looping Kedua')
var flag = 20;
while(flag > 1) { 
  console.log(flag+' - I Will become a mobile developer'); 
  flag-=2; 
}
console.log('=======================')
console.log('Soal 2 Looping Menggunakan for')
for (var x = 1; x < 21; x++) {
    if (x % 2 == 0){
      console.log( x + ' - Berkualitas' )
    }else if(x % 3 == 0) {
      console.log( x + ' - I Love Coding')
    }else if(x % 2 !== 0) {
      console.log( x + " - Santai") 
    }
}
console.log('=======================')
console.log('Soal 3 Membuat Persegi Panjang')
var persegi = '';
    for (var i = 1; i < 5; i++) {
        for (var j = 1; j < 9; j++) {
            persegi += '#';
        }
        persegi += '\n';
    }
    console.log(persegi);

console.log('=======================')
console.log('Soal 4 Membuat Tangga')
var tangga = '';
    for (var i = 1; i < 8; i++) {
        for (var j = 1; j <= i; j++) {
            tangga += '#';
        }
        tangga += '\n';
    }
console.log(tangga);

console.log('=======================')
console.log('Soal 5 Papan Catur')