console.log('Tugas Conditional')
console.log('Tugas If-Else:')

var nama = "Tulex"
var peran = " "

if(nama == "" && peran == "" ) {
    console.log("Nama Harus Diisi")
}else if(peran == " " ) {
    console.log('Halo ' + nama + ', Pilih Peranmu Untuk Memulai Game!')
}else if(peran == 'Penyihir') {
    console.log('Selamat Datang di Dunia Werewolf,' + nama)
    console.log('Halo Penyihir' + nama + ',Kamu dapat melihat siapa yang menjadi Werewolf') 
}else if(peran == 'Guard') {
    console.log('Selamat Datang di Dunia Werewolf,' + nama)
    console.log('Halo Guard' + nama + ',Kamu akan membantu melindungi temanmu')
}else if(peran == 'Werewolf') {
    console.log('Selamat Datang di Dunia Werewold.' + nama)
    console.log('Haalo Werewold' + nama + 'Kamu akan memakan mangsa setiap malam!') }

console.log('===================')
console.log('Tugas Switch Case :')

    var hari = 1;
    var Bulan = 5;
    var Tahun = 1945;

    switch(Bulan) {
        case 1:Bulan = 'Januari';break
        case 2:Bulan = 'Februari';break
        case 3:Bulan = 'Maret';break
        case 4:Bulan = 'April';break
        case 5:Bulan = 'Mei';break
        case 6:Bulan = 'Juni';break
        case 7:Bulan = 'Juli';break
        case 8:Bulan = 'Agustus';break
        case 9:Bulan = 'September';break
        case 10:Bulan = 'Oktober';break
        case 11:Bulan = 'November';break
        case 12:Bulan = 'Desember';break
    }

    console.log(hari + ' ' + Bulan + ' ' + Tahun + ' ')